package com.kumar.nitinTechOnline.gstbilling.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PriceUtilsTest {
    float finalPrice;
    int quantity;
    int taxSlab;
    PriceUtils priceUtils;

    @Before
    public void setUp() {
        finalPrice = 1;
        quantity = 2;
        taxSlab = 300;
        priceUtils = new PriceUtils(finalPrice, quantity, taxSlab);
        priceUtils.getRate();
        priceUtils.getTaxableValue();
        priceUtils.getSingleGst();
    }

    @Test
    public void getRateCuandoFinalPriceEsUno() {
        assertEquals(0.25, priceUtils.getRate(), 0.0001);
    }

    @Test
    public void getRateCuandoFinalPriceEsOcho() {
        priceUtils.setFinalPrice(8);
        assertEquals(2.0, priceUtils.getRate(), 0.0001);
    }

    @Test
    public void getTaxableValueTestDelEjemploPorDefecto() {
        assertEquals(0.5, priceUtils.getTaxableValue(), 0.0001);
    }

    @Test
    public void getTaxableValueTest() {
        assertEquals(0.5, priceUtils.getTaxableValue(), 0.0001);
    }

    @Test
    public void getTaxableValueSiQuantityEs10() {
        priceUtils.setQuantity(10);
        assertEquals(2.5, priceUtils.getTaxableValue(), 0.0001);
    }

    @Test
    public void gettaxableValueCuandoTaxEs19() {
        priceUtils.setTax(19);
        priceUtils.getRate();   // hace falta hacer esto para actualizar lo que se hace con tax
        assertEquals(0.1, priceUtils.getTaxableValue(), 0.0001);
    }

    @Test
    public void getSingleGstTest() {
        assertEquals(0.75, priceUtils.getSingleGst(), 0.0001);
    }

    @Test
    public void getSingleGstTestCuandoFinalPriceEs100() {
        priceUtils.setFinalPrice(300);
        priceUtils.getRate();
        priceUtils.getTaxableValue();
        assertEquals(225, priceUtils.getSingleGst(), 0.0001); // este es el correcto
        //assertEquals(1, priceUtils.getSingleGst(), 0.0001); // este tiene que dar error
    }
}